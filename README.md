## Recreated Issue around Entrypoint not working with dind

This repository recreates the issue highlighted here: https://gitlab.com/gitlab-org/gitlab-runner/issues/3404#note_96219478

You can see the runner configuration in - `config.toml`.

The `Dockerfile` is also included and the different versions of this are already in the registry of this project i.e. with entrypoint, without entrypoint.

---

**Notes:**

- `sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64`

- `sudo chmod +x /usr/local/bin/gitlab-runner`

- `gitlab-runner register`

- Follow the instructions in the command prompt - then go to `~/gitlab-runner/config.toml` and customise the configuration further to look like the `config.toml` file found in this project.

- Once we do this run `gitlab-runner verify`

- Then `gitlab-runner start`

- Lastly to debug the logs of the currently executing runner - `gitlab-runner -debug run`

You can now change the config.toml configuration on the fly and the changed will be reflected on GitLab.