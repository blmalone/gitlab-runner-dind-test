#!/bin/sh

set -e

if [[ -z "${SSH_KEY}" ]]; then
  echo "SSH key not specified."
else
  echo "Setting SSH keys into the SSH authentication agent."
fi

exec "$@"
