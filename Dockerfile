FROM docker:18.03.0-ce

ENV VERSION=0.2.10
ENV TERRA_VERSION 0.11.7

RUN apk update && \
    apk add \
        curl && \ 
    rm -rf /var/cache/apk/*

COPY docker-entrypoint.sh /usr/bin/

RUN chmod +x /usr/bin/docker-entrypoint.sh

#In registry.gitlab.com/blmalone/gitlab-runner-dind-test:no-entrypoint this line is commented out
ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]
